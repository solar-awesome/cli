#!/usr/bin/env node
const figlet = require("figlet");
const inquirer = require("./src/inquirer");
const { createProject, installProject, initGitRepo } = require("./src/project");
const { checkArgs } = require("./src/args");
const log = require("./src/lib/log");

log.info(figlet.textSync("Solar", { horizontalLayout: "full" }));

const run = async () => {
  const args = checkArgs(process.argv.slice(2));
  const { name, database, dockerDatabase } = await inquirer.askConfig();
  createProject(name, database, dockerDatabase, args);
  const { install } = await inquirer.askNpmInstall();
  if (install) {
    installProject(name, args);
  }
  const { git } = await inquirer.askGitRepoInit();
  if (git) {
    initGitRepo(name, args);
  }
  log.info(`Your microservice is created!`);
};

void run();