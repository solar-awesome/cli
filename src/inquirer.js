const inquirer = require("inquirer");
const fs = require("fs");

const baseQuestions = [
  {
    type: "input",
    name: "name",
    message: "Enter your microservice name: ",
    validate: (name) => {
      if (!name.trim().length) {
        return "Please enter microservice name.";
      } else if (fs.existsSync(name)) {
        return "Directory with this name already exists";
      }
      return true;
    }
  },
  {
    type: "confirm",
    name: "database",
    message: "Do you wanna add database layer?",
    default: true
  }
];

const dockerQuestions = [
  {
    type: "confirm",
    name: "dockerDatabase",
    message: "Do you wanna add docker-compose.yml for database?",
    default: true
  }
];

const askConfig = async () => {
  const { name, database } = await inquirer.prompt(baseQuestions);
  return {
    name,
    database,
    dockerDatabase: database
      ? (await inquirer.prompt(dockerQuestions)).dockerDatabase
      : false
  };
};

const askNpmInstall = () => {
  const questions = [
    {
      type: "confirm",
      name: "install",
      message: "Run npm install?",
      default: true
    }
  ];
  return inquirer.prompt(questions);
};

const askGitRepoInit = () => {
  const questions = [
    {
      type: "confirm",
      name: "git",
      message: "Init Git repo?",
      default: true
    }
  ];
  return inquirer.prompt(questions);
};

module.exports = {
  askConfig,
  askNpmInstall,
  askGitRepoInit
};