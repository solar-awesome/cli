const { createDir, createFile } = require("./lib/files");
const { createProto } = require("./generators/proto-generator");
const childProcess = require("child_process");
const log = require("./lib/log");
const { createMainIndex } = require("./generators/index-generator");
const { createPackageJson } = require("./generators/package-json-generator");
const { createNpmrc } = require("./generators/npmrc-generator");
const { createSrc } = require("./generators/src-generator");
const { createEnv } = require("./generators/env-generator");
const { createDb } = require("./generators/db-generator");
const { createDockerCompose } = require("./generators/docker-generator");

const createProject = (name, database, dockerDatabase, args) => {
  try {
    !args.includes("silent") && log.log("Generating project sources...");
    createDir(name);
    createPackageJson(name, database, dockerDatabase);
    createMainIndex(name);
    createProto(name);
    createSrc(name, database);
    createEnv(name, database);
    createNpmrc(name);
    if (database) {
      createDb(name);
      if (dockerDatabase) {
        createDockerCompose(name);
      }
    }
  } catch (e) {
    log.error(e);
    process.exit();
  }
};

const installProject = (name, args) => {
  try {
    !args.includes("silent") && log.log("Running npm install...");
    childProcess.spawnSync("npm", ["install"], {
      cwd: name,
      stdio: args.includes("silent") ? "ignore" : "inherit"
    });
  } catch (e) {
    log.error(e);
    process.exit();
  }
};

const initGitRepo = (name, args) => {
  try {
    !args.includes("silent") && log.log("Initing Git repo...");
    childProcess.spawnSync("git", ["init"], {
      cwd: name,
      stdio: args.includes("silent") ? "ignore" : "inherit"
    });
    createFile(name,
      ".gitignore",
      "node_modules\n" +
      ".env\n" +
      ".npmrc"
    );
    childProcess.spawnSync("git", ["add", "."], {
      cwd: name,
      stdio: args.includes("silent") ? "ignore" : "inherit"
    });
  } catch (e) {
    log.error(e);
    process.exit();
  }
};

module.exports = {
  initGitRepo,
  createProject,
  installProject
};