const fs = require("fs");

const createFile = (path, name, content) => {
	fs.writeFileSync(`${path}/${name}`, content);
};

const createDir = (name) => {
	fs.mkdirSync(name);
};

module.exports = {
	createDir,
	createFile
};