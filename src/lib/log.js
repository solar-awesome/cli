const chalk = require("chalk");

const info = (string) => {
	console.log(chalk.blueBright(string));
};

const log = (string) => {
	console.log(chalk.yellow(string));
};

const error = (string) => {
	console.log(chalk.red(string));
};

module.exports = {
	log,
	info,
	error
};