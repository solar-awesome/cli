const { createFile } = require("../lib/files");
const { capitalize } = require("../lib/string");

const createProto = (name) => {
	const proto = generateProto(name);
	createFile(name, `${name}.proto`, proto);
};

const generateProto = (name) =>
	`syntax = "proto3";

package ${name};

service ${capitalize(name)}Service {
	rpc get${capitalize(name)}s(Get${capitalize(name)}sRequest) returns (Get${capitalize(name)}sResponse) {};
	rpc get${capitalize(name)}ById(Get${capitalize(name)}ByIdRequest) returns (Get${capitalize(name)}ByIdResponse) {};
	rpc update${capitalize(name)}(Update${capitalize(name)}Request) returns (Update${capitalize(name)}Response) {};
	rpc create${capitalize(name)}(Create${capitalize(name)}Request) returns (Create${capitalize(name)}Response) {};
	rpc delete${capitalize(name)}(Delete${capitalize(name)}Request) returns (Delete${capitalize(name)}Response) {};
}

message ${capitalize(name)} {
	string id = 1;
	string createdAt = 2;
	string updatedAt = 3;
}

message Get${capitalize(name)}sRequest {
}

message Get${capitalize(name)}sResponse {
	repeated ${capitalize(name)} ${name}s = 1;
}

message Get${capitalize(name)}ByIdRequest {
	string id = 1;
}

message Get${capitalize(name)}ByIdResponse {
	${capitalize(name)} ${name} = 1;
}

message Update${capitalize(name)}Request {
	${capitalize(name)} ${name} = 1;
}

message Update${capitalize(name)}Response {
	${capitalize(name)} ${name} = 1;
}

message Create${capitalize(name)}Request {
}

message Create${capitalize(name)}Response {
	${capitalize(name)} ${name} = 1;
}

message Delete${capitalize(name)}Request {
	string id = 1;
}

message Delete${capitalize(name)}Response {
	${capitalize(name)} ${name} = 1;
}
`;

module.exports = {
	createProto
};
