const { createFile } = require("../lib/files");

const createEnv = (name, database) => {
  let content = basicEnv(name);
  if (database) {
    content = content
      .concat("\n\n")
      .concat(dbEnv(name));
  }
  createFile(name, ".env", content);
};

const basicEnv = (name) => `HTTP_PORT=3000
HTTP_HOST=localhost
HTTP_ENDPOINT=/proto
GRPC_HOST=localhost
GRPC_PORT=5000
SERVICE_NAME=${name}-gRPC`;

const dbEnv = (name) => `DATABASE_CLIENT=postgresql
DATABASE_HOST=localhost
DATABASE_PORT=5432
DATABASE_NAME=${name}-db
DATABASE_USER=postgres
DATABASE_PASS=postgres
DATABASE_MIGRATIONS_TN=knex_migration`;

module.exports = {
  createEnv
};