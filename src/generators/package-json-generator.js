const { createFile } = require("../lib/files");

const createPackageJson = (name, database, dockerDatabase) => {
  const content = `{
    "name": "${name}",
    "version": "1.0.0",
    "description": "gRPC microservice for ${name}s logic created with @solar/cli",
    "main": "index.js",
    "scripts": {
        "start": "node index.js",
        "start:dev": "nodemon index.js"${dockerDatabase ? `,
        "start:db": "docker-compose up -d"` : ""}${database ? `,
        "migrations": "knex migrate:latest"` : ""}
    },
    "dependencies": {
        "@solar/core-double": "1.0.1",
        "dotenv": "^10.0.0"${database ? `,
        "knex": "^2.1.0",
        "pg": "^8.7.3"` : ""}
    },
    "devDependencies": {
        "nodemon": "^2.0.20"
    }
}`;
  createFile(name, "package.json", content);
};


module.exports = { createPackageJson };