const { capitalize } = require("../lib/string");
const { createFile } = require("../lib/files");

const createGetAllMethod = (name) => {
	const content =
		`const { get${capitalize(name)}s } = require("../services/${name}-service");

	module.exports = async (ctx) => {
		const ${name}s = await get${capitalize(name)}s();
		ctx.res = { ${name}s };
	};`;
	createFile(`${name}/src/methods`, `get-${name}s.js`, content);
};

const createGetByIdMethod = (name) => {
	const content =
		`const { get${capitalize(name)}ById } = require("../services/${name}-service");

module.exports = async (ctx) => {
	const { id } = ctx.req;
	const ${name} = await get${capitalize(name)}ById(id);
	ctx.res = { ${name} };
};`;
	createFile(`${name}/src/methods`, `get-${name}-by-id.js`, content);
};

const createUpdateMethod = (name) => {
	const content =
		`const { update${capitalize(name)} } = require("../services/${name}-service");

module.exports = async (ctx) => {
	const { ${name} } = ctx.req;
	const updated${capitalize(name)} = (await update${capitalize(name)}(${name}))?.[0];
	ctx.res = { ${name}: updated${capitalize(name)} };
};`;
	createFile(`${name}/src/methods`, `update-${name}.js`, content);
};

const createCreateMethod = (name) => {
	const content =
		`const { create${capitalize(name)} } = require("../services/${name}-service");

module.exports = async (ctx) => {
	const { } = ctx.req;
	const ${name} = (await create${capitalize(name)}())?.[0];
	ctx.res = { ${name} };
};`;
	createFile(`${name}/src/methods`, `create-${name}.js`, content);
};

const createDeleteMethod = (name) => {
	const content =
		`const { delete${capitalize(name)} } = require("../services/${name}-service");

module.exports = async (ctx) => {
	const { id } = ctx.req;
	const ${name} = (await delete${capitalize(name)}(id))?.[0];
	ctx.res = { ${name} };
};`;
	createFile(`${name}/src/methods`, `delete-${name}.js`, content);
};

module.exports = {
	createGetAllMethod,
	createGetByIdMethod,
	createDeleteMethod,
	createUpdateMethod,
	createCreateMethod
};