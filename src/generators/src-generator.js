const { createDir } = require("../lib/files");
const { createService } = require("./service-generator");
const {
	createDeleteMethod,
	createCreateMethod,
	createUpdateMethod,
	createGetAllMethod,
	createGetByIdMethod
} = require("./methods-generator");
const { createMethodsIndex } = require("./index-generator");

const createSrc = (name, database) => {
	createSrcDir(name);
	createMethodsDir(name);
	createServiceDir(name, database);
};

const createSrcDir = (name) => {
	createDir(`${name}/src`);
};

const createServiceDir = (name, database) => {
	createDir(`${name}/src/services`);
	createService(name, database);
};

const createMethodsDir = (name) => {
	createDir(`${name}/src/methods`);
	createGetByIdMethod(name);
	createGetAllMethod(name);
	createUpdateMethod(name);
	createCreateMethod(name);
	createDeleteMethod(name);
	createMethodsIndex(name);
};


module.exports = { createSrc };