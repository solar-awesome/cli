const childProcess = require("child_process");
const { createFile, createDir } = require("../lib/files");
const createDb = (name) => {
  createDbSecrets(name);
  createKnexFile(name);
  createMigrations(name);
};

const createDbSecrets = (name) => {
  const content =
    `require("dotenv").config();
    
const secrets = {
  client: process.env.DATABASE_CLIENT,
  connection: {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    database: process.env.DATABASE_NAME,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS
  },
  pool: {
    min: 2,
    max: 20
  },
  migrations: {
    tableName: process.env.DATABASE_MIGRATIONS_TN
  }
};

module.exports = {
  secrets,
  db: require("knex")(secrets)
};`;
  createFile(`${name}/src`, "db.js", content);
};

const createKnexFile = (name) => {
  const content = `module.exports = require("./src/db").secrets;`;
  createFile(name, "knexfile.js", content);
};

const createMigrations = (name) => {
  createDir(`${name}/migrations`);
  createInitMigration(name);
};

const createInitMigration = (name) => {
  const content =
    `/**
 * @param { import("knex").Knex } knex
 * @returns {Knex.SchemaBuilder}
 */
exports.up = function(knex) {
  return knex.schema.createTable(
    "${name}s",
    function(t) {
      t.increments("id").primary();
      t.timestamp("created_at").defaultTo(knex.fn.now());
      t.timestamp("updated_at").defaultTo(knex.fn.now());
    }
  );
};

/**
 * @param { import("knex").Knex } knex
 * @returns {Knex.SchemaBuilder}
 */
exports.down = function(knex) {
  return knex.schema.dropTable("${name}s");
};
`;
  createFile(`${name}/migrations`, `${name}.js`, content);
};

module.exports = { createDb };