const { createFile } = require("../lib/files");

const createNpmrc = (name) => {
	const content = `@solar:registry=https://gitlab.com/api/v4/projects/41190768/packages/npm/`;
	createFile(name, ".npmrc", content);
};

module.exports = { createNpmrc };