const { capitalize } = require("../lib/string");
const { createFile } = require("../lib/files");

const createMethodsIndex = (name) => {
	const content = `const get${capitalize(name)}s = require("./get-${name}s");
const get${capitalize(name)}ById = require("./get-${name}-by-id");
const update${capitalize(name)} = require("./update-${name}");
const create${capitalize(name)} = require("./create-${name}");
const delete${capitalize(name)} = require("./delete-${name}");

module.exports = {
  get${capitalize(name)}ById,
  get${capitalize(name)}s,
  update${capitalize(name)},
  create${capitalize(name)},
  delete${capitalize(name)},
};`;
	createFile(`${name}/src/methods`, `index.js`, content);
};

const createMainIndex = (name) => {
	const content =
		`require("dotenv").config();
		
const SolarDouble = require("@solar/core-double");
const implementations = require("./src/methods");

new SolarDouble({ 
	protoPath: "./${name}.proto", 
	grpc: { implementations } 
}).start();
`;
	createFile(`${name}`, `index.js`, content);
};

module.exports = {
	createMainIndex,
	createMethodsIndex
};