const { capitalize } = require("../lib/string");
const { createFile } = require("../lib/files");

const createService = (name, database) => {
	database ? createDbService(name) : createBasicService(name);
};

const createBasicService = (name) => {
	const content =
		`const get${capitalize(name)}ById = async (id) => {
	return {
		id,
		createdAt: Date.now(),
		updatedAt: Date.now()
	};
};

const get${capitalize(name)}s = async () => {
	return [
		{
			id: 1,
			createdAt: Date.now(),
			updatedAt: Date.now()
		},
		{
			id: 2,
			createdAt: Date.now(),
			updatedAt: Date.now()
		}
	];
};

const create${capitalize(name)} = async () => {
	return [{
		id: 1,
		createdAt: Date.now(),
		updatedAt: Date.now()
	}];
};

const update${capitalize(name)} = async (${name}) => {
	return [${name}];
};

const delete${capitalize(name)} = async (id) => {
	return [{
		id,
		createdAt: Date.now(),
		updatedAt: Date.now()
	}];
};

module.exports = {
	get${capitalize(name)}ById,
	get${capitalize(name)}s,
	create${capitalize(name)},
	update${capitalize(name)},
	delete${capitalize(name)}
};`;
	createFile(`${name}/src/services`, `${name}-service.js`, content);
};

const createDbService = (name) => {
	const content =
		`const db = require("../db").db;

const ${name}Aliases = [
  "id",
  "created_at as createdAt",
  "updated_at as updatedAt"
];

const get${capitalize(name)}ById = async (id) => {
  return db("${name}s")
    .select(${name}Aliases)
    .where({ id })
    .first();
};

const get${capitalize(name)}s = async () => {
  return db("${name}s")
    .select(${name}Aliases);
};

const create${capitalize(name)} = async () => {
  return db("${name}s")
    .insert({})
    .returning(${name}Aliases);
};

const update${capitalize(name)} = async (${name}) => {
  return db("${name}s")
    .update({ updated_at: db.fn.now() })
    .where({ id: ${name}.id })
    .returning(${name}Aliases);
};

const delete${capitalize(name)} = async (id) => {
  return db("${name}s")
    .delete()
    .where({ id })
    .returning(${name}Aliases);
};

module.exports = {
  get${capitalize(name)}ById,
  get${capitalize(name)}s,
  create${capitalize(name)},
  update${capitalize(name)},
  delete${capitalize(name)}
};`;
	createFile(`${name}/src/services`, `${name}-service.js`, content);
};


module.exports = { createService };