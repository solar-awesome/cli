const log = require("./lib/log");
const supportedArgs = [
  "--silent",
  "--help"
];

const checkArgs = (args) => {
  const checkedArgs = [];
  for (let i in args) {
    if (!supportedArgs.includes(args[i])) {
      console.log(unknownArgMessage(args[i]));
      process.exit();
    } else {
      if (args[i] === "--help") {
        log.log(helpMessage);
        process.exit();
      } else {
        checkedArgs.push(args[i].slice(2));
      }
    }
  }
  return checkedArgs;
};

const unknownArgMessage = (arg) => `Unknown command: "${arg}"\n
To see a list of supported commands, run:
  solar --help`;

const helpMessage =
  `name: solar
description: Easy to use CLI for creating gRPC microservices with HTTP-shared .proto
usage: solar [...args]
arguments:
  --silent: removes all log of internal execution
  --help: show help`;

module.exports = { checkArgs };